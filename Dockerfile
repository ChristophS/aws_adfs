from python:3.6.5

# Check if changes in files 
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

WORKDIR /workspace

COPY adfs.py /adfs.py
COPY auth /usr/local/bin/auth
RUN chmod a+x /usr/local/bin/auth
CMD [ "/bin/bash" ]