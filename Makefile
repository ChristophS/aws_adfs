IMAGE ?= adfs_aws
build:
	docker build . -t $(IMAGE)
shell:
	docker run -it -v ~/.aws/:/root/.aws/ $(IMAGE) 
refresh:
	docker run -it -v ~/.aws/:/root/.aws/ --entrypoint auth $(IMAGE) 
