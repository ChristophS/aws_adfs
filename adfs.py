#https://gist.github.com/s-fujimoto/9f620e859f727d891770
#https://aws.amazon.com/blogs/security/how-to-implement-federated-api-and-cli-access-using-saml-2-0-and-ad-fs/

import argparse
import base64
from configparser import ConfigParser
import os
import xml.etree.ElementTree as ET
from getpass import getpass
from os.path import expanduser
from datetime import datetime, timedelta
import pytz
import sys
import boto3
import requests
import urllib3
from bs4 import BeautifulSoup as bs

urllib3.disable_warnings()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--adfs-fqdn", required=True, dest="ADFS_FQDN")
    parser.add_argument("--username", required=True, dest="USERNAME")
    parser.add_argument("--force", action='store_false')
    parser.add_argument(
        "--duration", required=False, default=3600, dest="DURATION")
    parser.add_argument(
        "--region", required=False, default='eu-west-1', dest="REGION")
    parser.add_argument(
        "--output", required=False, default='json', dest="OUTPUT")
    parser.add_argument(
        "--config-file",
        required=False,
        default="~/.aws/credentials",
        dest="CONFIG_FILE")

    args = parser.parse_args()
    if args.force and credentials_still_valid(filename=args.CONFIG_FILE):
        return True

    password = getpass(args.USERNAME + "'s Password: ")
    response = request(args.ADFS_FQDN, args.USERNAME, password)
    soup = bs(response.text, 'html.parser')
    saml_assertion = soup.find(attrs={"name": "SAMLResponse"})["value"]

    for awsrole in get_aws_roles(saml_assertion):
        role_arn, principal_arn = awsrole
        credentials = get_security_token(
            role_arn=role_arn,
            principal_arn=principal_arn,
            saml_assertion=saml_assertion,
            duration=args.DURATION,
        )
        profile_name = get_profile_name(role_arn)
        print_info(profile_name, credentials.get("Expiration"))
        
        write_profile(
            profile_name,
            credentials,
            region=args.REGION,
            output=args.OUTPUT,
            filename=args.CONFIG_FILE)


def request(adfs_name, username, password):
    url = 'https://' + adfs_name + '/adfs/ls/IdpInitiatedSignOn.aspx?loginToRp=urn:amazon:webservices'

    data = {
        "UserName": username,
        "Password": password,
        "Kmsi": "true",
        "AuthMethod": "FormsAuthentication"
    }
    result = requests.post(
        url,
        data,
        verify=False,
        headers={'Content-Type': "application/x-www-form-urlencoded"})
    if 'SAMLResponse' not in result.text:
        print("ERROR: You entered the wrong username/password! Exiting script...")
        sys.exit(1)

    return result


def print_info(profile, duration):
    now = datetime.now(tz=pytz.utc)
    delta = duration - now
    print("{profile} - {duration}".format(profile=profile, duration=delta))


def get_aws_roles(saml_assertion):
    # Parse the returned assertion and extract the authorized roles
    awsroles = []
    root = ET.fromstring(base64.b64decode(saml_assertion))

    def extract_role_principle_arn(awsrole):
        chunks = awsrole.split(',')
        if 'saml-provider' in chunks[0]:
            newawsrole = chunks[1] + ',' + chunks[0]
            role_arn = newawsrole.split(',')[0]
            principle_arn = newawsrole.split(',')[1]
        return (role_arn, principle_arn)

    for saml2attribute in root.iter(
            '{urn:oasis:names:tc:SAML:2.0:assertion}Attribute'):
        if (saml2attribute.get('Name') ==
                'https://aws.amazon.com/SAML/Attributes/Role'):
            for saml2attributevalue in saml2attribute.iter(
                    '{urn:oasis:names:tc:SAML:2.0:assertion}AttributeValue'):
                awsrole = extract_role_principle_arn(saml2attributevalue.text)
                awsroles.append(awsrole)

    return awsroles


def get_security_token(role_arn, principal_arn, saml_assertion, duration):
    if isinstance(duration, str):
        duration = int(duration)

    client = boto3.client('sts')

    result = client.assume_role_with_saml(
        RoleArn=role_arn,
        PrincipalArn=principal_arn,
        SAMLAssertion=saml_assertion,
        DurationSeconds=duration)
    return result['Credentials']


def get_profile_name(role_arn):
    account_id = role_arn.split(':')[4]
    role_name = role_arn.split(':')[5].split('/')[1]
    return account_id + "_" + role_name


def write_profile(profile_name, credentials, region, output, filename):

    # Write the AWS STS token into the AWS credential file
    filename = expanduser(filename)

    # Read in the existing config file
    config = ConfigParser()
    config.read(filename)

    # Put the credentials into a specific profile instead of clobbering
    # the default credentials
    if not config.has_section(profile_name):
        config.add_section(profile_name)

    config.set(profile_name, 'output', output)
    config.set(profile_name, 'region', region)
    config.set(profile_name, 'aws_access_key_id', credentials['AccessKeyId'])
    config.set(profile_name, 'aws_secret_access_key',
               credentials['SecretAccessKey'])
    config.set(profile_name, 'aws_session_token', credentials['SessionToken'])
    config.set(profile_name, 'expiration',
               str(credentials['Expiration'].timestamp()))

    # Write the updated config file
    with open(filename, 'w+') as configfile:
        config.write(configfile)


def credentials_still_valid(filename):
    # Write the AWS STS token into the AWS credential file
    filename = expanduser(filename)

    # Read in the existing config file
    config = ConfigParser()
    config.read(filename)
    for section in config.sections():
        try:
            expiration = config.get(section, 'expiration')
            expiration = float(expiration)
            expiration = datetime.fromtimestamp(expiration, pytz.utc)
            time_to_expiration = expiration - datetime.now(tz=pytz.utc)
            if time_to_expiration > timedelta(minutes=5):
                print("Credentials {profile} valid for {delta}".format(
                    profile=section, delta=time_to_expiration))
                return True
        except Exception:
            pass
    return False


if __name__ == "__main__":
    main()
