
# Get AWS Credentials via ADFS
## Description
This tool is used to log in to ADFS to get a SAML response. Using this SAML response at AWS to create short-term access data and write it to the ~/.aws/credentials.


## Installation ZSH
Replace "--adfs-fqdn adfs.example.com"
Replace "--username  example\\bob"

```
echo 'function awscred() {
  docker run -it \
    --volume=$PWD:/workspace -v ~/.aws:/root/.aws \
    --entrypoint auth registry.gitlab.com/christophs/aws_adfs:latest \
    --adfs-fqdn adfs.example.com --username example\\bob "$@" 
}
' >> ~/.zshrc
```

## Using the awscred command

'''
awscred 
'''
